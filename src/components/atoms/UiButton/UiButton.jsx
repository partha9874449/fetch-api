import React from 'react'
import PropTypes from 'prop-types'
import './styles.scss'

export const UiButton = ({ className, children, onClickHandler }) => (
  <button onClick={onClickHandler} className={className}>
    {children}
  </button>
)

UiButton.propTypes = {
  children: PropTypes.any.isRequired,
  className: PropTypes.string,
  onClickHandler: PropTypes.func
}

UiButton.defaultProps = {
  onClickHandler: () => {},
  className: ''
}
